
public class Main {

	public static void main(String[] args) {
		new Main();

	}
	
	/**
	 * Start a March Madnes round
	 */
	public Main() {
		
		
		Prediction[] predictions = new Prediction[]{
				new Prediction("North Carolina", 1),
				new Prediction("Broncos", 63),
				new Prediction("LA Lakers", 3),
				new Prediction("Virginia", 2),
				new Prediction("Kentucky", 2),
				new Prediction("Hockey", 64),
				new Prediction("Xavier", 4),
				new Prediction("Buffalo", 1),
				new Prediction("Creighton", 16),
				new Prediction("Kansas", 1),
				new Prediction("Iowa", 1),
				new Prediction("Duke", 5),
				new Prediction("UNO", 6)
				};
		
		printGeneric(predictions);
		
		String[] partyFood = new String[]{"Nachos", "Pizza", "Hot Wings", "Cake", "Chips", "Brownies", "A giant sandwich"};
		
		System.out.println("Here's the food we're going to bring.");
		
		printGeneric(partyFood);
		
		
		//int[] highestScoringGame = new int[]{186, 212, 155, 160, 21, 190, 193, 600, 204};
		Integer[] highestScoringGame = new Integer[]{186, 212, 155, 160, 21, 190, 193, 600, 204};
		
		System.out.println("Here are our predictions for the combined highest score this year:");
		
		printGeneric(highestScoringGame);
		
		/**
		 * How to use generics
		 * 1. Add the generic tpe when you call the generic object
		 */
		ArrayList<String> biggestUpsets = new ArrayList<String>();
		biggestUpsets.add("Kansas");
		biggestUpsets.add("Oregon");
		biggestUpsets.add("Buffalo");
		biggestUpsets.add("Michigan State");
		
		biggestUpsets.print();
		
		String firstUpset = biggestUpsets.get(0);
		
		/* Things to know about generics:
		 * They can have ant number of generic type parameters
		 * <T,U,V>
		 * 
		 * Map<String, Integer>
		 * 
		 * 
		 * You don't have to call a generic class with <String> or whatever
		 * You have to do a lot of casting
		 * 
		 */
		
		
		ArrayList springBreakDestinations = new ArrayList();
		springBreakDestinations.add("Bed");
		springBreakDestinations.add("Texas");
		springBreakDestinations.add("San Diego");
		
		/*
		 * Problem 1 is lots of casting
		 */
		String destination = (String)springBreakDestinations.get(2);
		
		/*
		 * Problem 2, I can add whatever I want
		 */
		
		springBreakDestinations.add(new Prediction("49ers", 1));
		
		/*
		 * Last thing
		 * 
		 * When you declare a parameterized object...
		 * public class ArrayList<T extends Comparable>
		 * where comparable is the type all instantiation have to inherit from
		 */
		
		
		
		
		
		/**
		 * Introducing: generics 
		 */
		
		
		
	}

	/*
	 * A generic means Java figures out at compile time which type it really is.
	 * 1. Add a generic declaration before the return statement
	 * (private <T> void pintGeneric)
	 * The name in <> is he generic type
	 * 
	 * 2. Reference the generic type parameter in the method signature
	 * private <T> void printGeneric(T[] array) {
	 */
	
	/*Boxing and unboxing
	 * 
	 * Boxing->primitive type to Object
	 * Unboxing ->Object type to primitive
	 * 
	 * int -> Integer()
	 * double -> Double
	 * 
	 * Every single primitive type has a corresponding Object type
	 * 
	 */
	
	
	private <T> void printGeneric(T[] array) {
		for(int i = 0; i < array.length; i++)
			System.out.println(array[i]);
		
	}

}
