/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Michelangelo.
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.MichelangeloDefendingChuck
 * 
 * When defending against Darth Vader, this class should return HelperStrings.MichelangeloDefendingDarthVader
 * @author bricks
 *
 */
public class MichelangeloTheNinjaTurtle extends AContestant{
	
	public MichelangeloTheNinjaTurtle()
	{
		super("Michelangelo");
	}
	
	@Override
	public String defendAgainst(IContestant contestant) {
		if(contestant instanceof ChuckNorris)
			return HelperStrings.MichelangeloDefendingChuck;
		///else
		return HelperStrings.MichelangeloDefendingDarthVader;
	}
	

}
