/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Darth Vader.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.DarthVaderDefendingMichelangelo
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.DarthVaderDefendingChuckNorris
 * @author bricks
 *
 */
public class DarthVader extends AContestant {

	public DarthVader()
	{
		super("Darth Vader");
		
	}
	
	@Override
	public String defendAgainst(IContestant contestant) {
		if(contestant instanceof MichelangeloTheNinjaTurtle)
			return HelperStrings.DarthVaderDefendingMichelangelo;
		///else
			return HelperStrings.DarthVaderDefendingChuckNorris;
	}
	
}
