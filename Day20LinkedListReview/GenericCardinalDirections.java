/**
 * A class that maintains a reference to our current location.
 * @author unouser
 *
 * @param <T>
 */
public class GenericCardinalDirections<T> {
	
	public Node<T> whereIAm;
	
	public GenericCardinalDirections(Node<T> startNode) {
		
		whereIAm = startNode;
	}
	
	public Node<T> moveNorth()
	{
		if(whereIAm.North == null)
			return null;
		else
		{
			whereIAm = whereIAm.North;
			return whereIAm;
		}
	}

	public Node<T> moveEast() {
		if(whereIAm.East == null)
			return null;
		else
		{
			whereIAm = whereIAm.East;
			return whereIAm;
		}
		
	}

	public Node<T> moveSouth() {
		if(whereIAm.South == null)
			return null;
		else
		{
			whereIAm = whereIAm.South;
			return whereIAm;
		}
		
	}

	public Node<T> moveWest() {
		if(whereIAm.West == null)
			return null;
		else
		{
			whereIAm = whereIAm.West;
			return whereIAm;
		}
		
	}
	
	
	

}
