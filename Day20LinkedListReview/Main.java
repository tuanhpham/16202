/**
 * The entry point for our program
 * @author unouser
 *
 */
public class Main {

	/**
	 * The entry point for the program.
	 * @param args Args we ignore
	 */
	public static void main(String[] args) {
		new Main();

	}
	
	/**
	 * Constructor for our main class. Create the cardinal setup
	 */
	public Main() {
		
		/* Instantiate our locations */
		Node<String> dirtPatch = new DirtPatch();
		Node<String> iceRink = new IceRink();
		Node<String> Circus= new Circus();
		Node<String> Detroit = new Detroit();
		Node<String> GolfCourse = new GolfCourse();
		Node<String> hanoiHilton = new HanoiHilton();
		Node<String> hollywood = new Hollywood();
		Node<String> LavaPool = new LavaPool();
		Node<String> moneyLand = new MoneyLand();
		Node<String> movieTheatere = new MovieTheatre();
		Node<String> narnia = new Narnia();
		Node<String> peanutFarm = new PeanutFarm();
		Node<String> sanDiego = new SanDiego();
		Node<String> themoon = new theMoon();
		Node<String> tree = new Tree();
		
		iceRink.East = Circus;
		Circus.North = Detroit;
		Detroit.West = GolfCourse;
		GolfCourse.North = hanoiHilton;
		hanoiHilton.South = moneyLand;

		
		
		/* Hook everything together */
		dirtPatch.North = iceRink;
		iceRink.South = dirtPatch;
		
		
		GenericCardinalDirections<String> gcd = new GenericCardinalDirections<>(dirtPatch);
		
		java.util.Scanner scanner = new java.util.Scanner(System.in);
		
		/* Create a game loop */
		while(true)
		{
			System.out.println(gcd.whereIAm.myData);
			System.out.println();
			System.out.println("Where do you want to go today?");
			
			String input = scanner.next();
			
			if(input.equals("n"))
			{
				if(gcd.moveNorth() == null)
					System.out.println("This disco no worko.");
			}
			if(input.equals("e"))
				if(gcd.moveEast() == null)
					System.out.println("This disco no worko.");
			if(input.equals("s"))
				if(gcd.moveSouth() == null)
					System.out.println("This disco no worko.");
			if(input.equals("w"))
				if(gcd.moveWest() == null)
					System.out.println("This disco no worko.");
			if(input.equals("exit"))
				break;
			
			
		}
		System.out.println("The game is over.");
	}

}
