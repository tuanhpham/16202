/**
 * A class that represents a node with connections to cardinal directions.
 * @author unouser
 *
 * @param <T>
 */
public class Node<T> {
	
	public Node<T> North = null;
	public Node<T> East = null;
	public Node<T> South = null;
	public Node<T> West = null;
	
	public T myData = null;
	
	public Node(T inT) {
		myData = inT;
	}
	

}
