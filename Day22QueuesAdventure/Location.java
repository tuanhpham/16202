import java.util.ArrayList;
import java.util.List;

public class Location extends GameObject {
	
	
	private Location north = null;
	private Location east = null;
	private Location south = null;
	private Location west = null;
	
	private List<PhysicalObject> physicalObjects = new ArrayList<PhysicalObject>();
	
	public Location(String inName, String inDescription) {
		super(inName, inDescription);
	}
	
	/**
	 * @return the north
	 */
	public Location getNorth() {
		return north;
	}

	/**
	 * @param north the north to set
	 */
	public void setNorth(Location north) {
		this.north = north;
	}

	/**
	 * @return the east
	 */
	public Location getEast() {
		return east;
	}

	/**
	 * @param east the east to set
	 */
	public void setEast(Location east) {
		this.east = east;
	}

	/**
	 * @return the south
	 */
	public Location getSouth() {
		return south;
	}

	/**
	 * @param south the south to set
	 */
	public void setSouth(Location south) {
		this.south = south;
	}

	/**
	 * @return the west
	 */
	public Location getWest() {
		return west;
	}

	/**
	 * @param west the west to set
	 */
	public void setWest(Location west) {
		this.west = west;
	}

	/**
	 * @return the physicalObjects
	 */
	public List<PhysicalObject> getPhysicalObjects() {
		return physicalObjects;
	}
}
