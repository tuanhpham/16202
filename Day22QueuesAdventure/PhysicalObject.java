
public abstract class PhysicalObject extends GameObject {

	
	private String inlineDescription;
	
	private Location connection = null;
	
	public PhysicalObject(String inName, String inDescription, String quickDescription) {
		super(inName, inDescription);
		setInlineDescription(quickDescription);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the inlineDescription
	 */
	public String getInlineDescription() {
		return inlineDescription;
	}

	/**
	 * @param inlineDescription the inlineDescription to set
	 */
	private void setInlineDescription(String inlineDescription) {
		this.inlineDescription = inlineDescription;
	}
	
	public PhysicalObject addConnection(Location inConnection)
	{
		connection = inConnection;
		return this;
	}
	
	public Location getConnection()
	{
		return connection;
	}

}
