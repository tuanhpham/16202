public class GameController {
	
	Location last; 
	Location me;  
	
	boolean isLive = true;
	
	/** Keep track of where I've been, so I can undo. */
	Stack<Location> undoStack = new Stack<Location>();
	
	public GameController() {
		
		
		Location start = new Location("Classroom", "You are in what used to be a classroom. All around you computers and sizzling and desks are burnt. There clearly used to be a lot of people in here since the are cellphones and backpacks scattered everywhere. ");
		
		Location hall = new Location("Hall", "You stand in a main hallway. Looking at your cellphone's compass, it's clear that the hall runs north and south. In the distance you hear the screams of students. Probably the students who just fled from the classroom.");
		
		Location southHall = new Location("South Hall", "You stand at the south end of a hallway, which ends in a large pane of glass. Hovering above the parkinglot outside is a large UFO. You feel really jealous that you weren't chosen to represent earth in the intergalactic games. Your resolve to get on the UFO only grows.");
		
		Location northHall = new Location("North Hall", "You stand at the north end of the hallway. When you came to class this hallways led to a large staircase next to a large glass sculpture. Neither of them exist anymore, since they too seemed to be vaporized by the appearance of the UFOs. The screaming of students is louder here. Several of them seem to have climbed down the rubble to the main floor. ");
		
		Location inUFO = new Location("UFO", "You start to climb down the rubble, but as you do, the rubble starts to slide and soon to you are falling uncontrollably. You cut your hands on the remains of the glass sculpture and finally run into one of the now-bare beams that supports the building. The building shakes, trembles, and then slowly begins to collapse. Just as a large chunk of building is about to hit you, a UFO appears and beams into the main cabin. \r\n\r\nYou wake up after what feels like a long time later. You are floating in a large tube full of a thick liquid. In front of your two aliens are talking to each other as they drive the UFO. 'Why did you go back to get that student?' one asks.\r\nThe other shrugs, 'The was a very graceful fall. I liked it.' \r\nJust then a baby alien waddles in. \r\n'Daddy, are we there yet?' \r\n'No, no darling, it will be at least another three hundred years.'");
		
		
		hall.setSouth(southHall);
		hall.setNorth(northHall);
		
		southHall.setNorth(hall);
		
		northHall.setSouth(hall);
		
		start.getPhysicalObjects().add(new StaticObject("Door", "There's nothing unusual about this door. It's just a door.", "The door to the room stands ajar leading to the main hall. ").addConnection(hall));
		hall.getPhysicalObjects().add(new StaticObject("Door", "There's nothing unusual about this door. It's just a door.", "There is a door in this hallway that seems to lead into a classroom. ").addConnection(start));
		northHall.getPhysicalObjects().add(new StaticObject("Rubble", "The rubble looks dangerous, but you might be able to climb down", "If you tried, you might be able to climb down the rubble yourself.").addConnection(inUFO));
		
		
		last = null;
		me = start;
		
		java.util.Scanner scanner = new java.util.Scanner(System.in);
		
		Queue<String> queue = new Queue<String>();
		queue.add("goback");
		queue.add("door");
		queue.add("n");
		queue.add("rubble");
		queue.add("door");
		queue.add("goback");
		queue.add("goback");
		queue.add("s");
		queue.add("goback");
		queue.add("door");
		queue.add("goback");
		queue.add("e");
		
		
		while(true)
		{
			System.out.print(me.getDescription());
			for(PhysicalObject po : me.getPhysicalObjects())
			{
				System.out.print(po.getInlineDescription());
			}
			System.out.println();
			System.out.println();
			
			System.out.print(me.getName() + " >");
			
			String response;
			
			if(isLive)
			{
				response = scanner.next();
			}
			else{
				if(queue.isEmpty())
				{
					System.out.println("Done testing.");
					return;
				}
				
				response = queue.pop();
			}
			System.out.println();
			
			
			if(response.equals("goback"))
			{
				if(undoStack.isEmpty())
					System.out.println("Hey, you can't go back.");
				else
					me = undoStack.pop();
			}
			else if(response.equals("e"))
			{
				if(me.getEast() == null)
					System.out.println("Can't go that way.");
				else
					move(me.getEast());
			}
			else if(response.equals("n"))
			{
				if(me.getNorth() == null)
					System.out.println("Nope, can't do that.");
				else
					move(me.getNorth());
			}
			else if(response.equals("w"))
			{
				if(me.getWest() == null)
					System.out.println("That was is totally impassible.");
				else
					move(me.getWest());
			}
			else if(response.equals("s"))
			{
				if(me.getSouth() == null)
					System.out.println("Yeah, that's not going to work out so well.");
				else
					move(me.getSouth());
			}
			else
			{
				boolean foundWhatToDo = false;
				
				for(PhysicalObject po : me.getPhysicalObjects())
				{
					if(po.getName().toLowerCase().equals(response))
					{
						foundWhatToDo = true;
					
						if(po instanceof StaticObject)
						{
							Location connection = ((StaticObject)po).getConnection();
							if(connection != null)
								move(connection);
							else
								System.out.println(po.getDescription());
						}
					}
				}
				
				if(!foundWhatToDo)
				{
					System.out.println("I didn't get that. Why don't you try again.");
				}
			}
		}
		
	}

	/**
	 * Move the me reference to the given location
	 * @param connection the new location for me
	 */
	private void move(Location connection) {
		undoStack.add(me);
		me = connection;
		
	}

}
