/**
 * Linked List example
 * From Day 19
 * @author unouser
 *
 */
public class Main {
	
	/**
	 * Hold references to the start and end nodes
	 * @author unouser
	 *
	 */
	public class LinkedList
	{
		/** The beginning of our linkd list */
		private Node startNode = null;
		
		/**The end of our linked list */
		private Node endNode = null;
		
		/**
		 * Add a new node to the end of our list
		 * @param adNode
		 */
		void add(Node addNode)
		{
			//Check to see if I'm empty
			if(startNode == null)
			{
				//Have the art and end point to the new node
				startNode = addNode;
				endNode = addNode;
			}
			else
			{
				//We need to add a node to the end
				//1. We need the last node to change its "next" to be addNode
				//2. Update the endNode reference to addNode
				endNode.next = addNode;
				endNode = endNode.next;
			}
		}
		
		/**
		 * Count how mant nodes we have
		 * @return
		 */
		public int size()
		{
			//1. Start at start node
			//2. loop until we hit o end node
			
			//Where we are in the linked list
			Node currentNode = startNode;
			
			//Count how many nodes we've seen
			int count = 0;
			
			//Loop until we get to the end
			while(currentNode != null)
			{
				count++;
				currentNode = currentNode.next;
			}
			
			//Return how many nodes we've seen
			return count;
		}

		/**
		 * Remove a node that has string as data
		 * @param string
		 */
		public void remove(String string) {
			//1. Find it
			//2. Remove it
			//a. You need special cases for the first and last nodes
			
			
			//Find it
			Node previousNode = startNode;
			//Stop when the next node is the one I want
			while(!previousNode.next.data.equals(string))
			{
				previousNode = previousNode.next;
			}
			
			//delete it
			previousNode.next = previousNode.next.next;
			
			
			
		}
	}
	
	/**
	 * Every node in a linked list needs two things: data, and a reference to the next node
	 * @author unouser
	 *
	 */
	public class Node
	{
		/** The data this node holds */
		public String data;
		
		/**A reference to the next node, null if there is none */
		public Node next = null;
		
		/**
		 * A constructor that takes a string reference 
		 * @param inData
		 */
		public Node(String inData){data = inData;}
		
	}

	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		LinkedList mainList = new LinkedList();
		mainList.add(new Node("279"));
		System.out.println(mainList.size());
		mainList.add(new Node("278"));
		System.out.println(mainList.size());
		mainList.add(new Node("300"));
		System.out.println(mainList.size());
		mainList.remove("279");
		
	}

}
