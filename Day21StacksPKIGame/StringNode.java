/**
 * A class that extends Node<T> where T is of type string
 * @author unouser
 *
 */
public class StringNode extends Node<String>{

	public StringNode(String inData) {
		super(inData);
	}

}
