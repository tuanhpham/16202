/**
 * A class that point the four cardinal directions and has data
 * @author unouser
 *
 */
public class Node<T> {
	
	public Node<T> North;
	public Node<T> East;
	public Node<T> South;
	public Node<T> West;
	
	T data = null;
	
	public Node(T inData) {
		data = inData;
	}
	
	

}
