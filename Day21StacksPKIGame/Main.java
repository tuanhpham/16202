import java.util.ArrayList;
import java.util.List;


/**
 * The main class for the project
 * @author unouser
 *
 */
public class Main {

	/**
	 * The entry point to our program
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to the PKI game");
		
		new Main();

	}
	
	
	public Main() {
		StringNode pki = new StringNode("PKI is a multifloor building.");
		StringNode dirt = new StringNode("Rich in nutrients and dust.");
		StringNode cafe = new StringNode("Taco Tuesday! Grab some grub.");
		StringNode paris = new StringNode("There�s this big tower.");
		StringNode park = new StringNode("A park full of flowers, trees, and gum-covered benches.");
		StringNode parkingLot = new StringNode("Parking lot with no parking!");     
		StringNode pki2 = new StringNode("Computer Science");
		StringNode bathroom = new StringNode("The bathrooms on campus are so nice");
		StringNode Omaha = new StringNode("Omaha is a great city"); 
		StringNode cavern = new StringNode("A spooky cave full of dancing skeletons.");
		StringNode riverStix = new StringNode("River Stix, the dark underworld");    
		  pki.East = dirt;
		  dirt.West = pki;
		  dirt.East = cafe;
		  cafe.West = dirt;
		  cafe.South = riverStix;
		  cafe.East = paris;
		  park.South = cavern;
		  cavern.South = riverStix;
		parkingLot.North = parkingLot;
		StringNode firstData = new StringNode("To the West of PKI.");
		firstData.West = firstData;
		//dirt.East = neighborhood;
		StringNode golf = new StringNode("You�re at the top of a hill looking down at hole Number One");
		park.East = golf;
		StringNode MammellHall = new StringNode("You�re at the large building south of PKI");
		StringNode home = new StringNode("Home is where the heart is.");
		    Omaha.South = home;
		


		StringNode cathluTheGreatOne= new StringNode("AHLEKJFJA;WEKLJF");
		dirt.North = cathluTheGreatOne;

		StringNode neighborhood = new StringNode("a friendly neighborhood");
		StringNode maxwellHome = new StringNode("Swords await");
		StringNode creek = new StringNode("Filled with water and seaweed.");


		bathroom.South = cavern;
		StringNode busStop = new StringNode("students standing next to scary homeless people");
		//creek.North = bustop;


		
		Node<String> me = pki;
		
		recurse(me);
		
		java.util.Scanner scanner = new java.util.Scanner(System.in);
		
		while(true)
		{
			System.out.println(me.data);
			System.out.println("Where do you want to go today?");
			
			String response = scanner.next();
			
			if(response.equals("e"))
			{
				if(me.East == null)
					System.out.println("You shall not pass.");
				else
					me = me.East;
			}
			if(response.equals("n"))
			{
				if(me.North == null)
					System.out.println("You shall not pass.");
				else
					me = me.North;
			}
			if(response.equals("w"))
			{
				if(me.West == null)
					System.out.println("You shall not pass.");
				else
					me = me.West;
			}
			if(response.equals("s"))
			{
				if(me.South == null)
					System.out.println("You shall not pass.");
				else
					me = me.South;
			}
				
			
		}
		
	}


	List<Node<String>> visted = new ArrayList<Node<String>>();
	
	private void recurse(Node<String> current) {
		
		if(current == null)
			return;
		
		visted.add(current);
		
		if(current.North != null)
			if(current.North.South == null)
				current.North.South = current;
		if(current.South != null)
			if(current.South.North == null)
				current.South.North = current;
		if(current.East != null)
			if(current.East.West == null)
				current.East.West = current;
		if(current.West != null)
			if(current.West.East == null)
				current.West.East = current;
		
		if(!visted.contains(current.North))
		{
			recurse(current.North);
		}if(!visted.contains(current.South))
		{
			recurse(current.South);
		}if(!visted.contains(current.East))
		{
			recurse(current.East);
		}if(!visted.contains(current.West))
		{
			recurse(current.West);
		}
		
	}

}
