
public class NightHourlyBonus extends Bonus{
	
	public NightHourlyBonus() {
		providesFood = -4;
		providesWater = -2;
		providesRest = -3;
		providesHeat = -8;
	}

}
