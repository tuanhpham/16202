
public class BuildFireActivity extends Activity{
	
	public BuildFireActivity() {
		
		bonus.providesHeat += 30;
		bonus.providesRest -= 10;
		probabilityOfSuccess = .8;
		name = "Build Fire";
		HoursRequired = 1;
		
	}

}
