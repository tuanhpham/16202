
public class Attacker extends Hero {

	public Attacker() {
		super(100, 20, 2, 2, "Throw an enormous snow ball", "Throw snow balls at all opponents.");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void special(Player me, Player opponent) {
		for(Hero hero : opponent.heroes)
		{
			hero.takeDamage(15);
		}

	}

}
