import java.awt.Dimension;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements View{
	
	public MainFrame() {
		super("A Snowball Fight!");
		
		Main.model.addView(this);
		
		this.setVisible(true);
		
		this.setMinimumSize(new Dimension(400, 400));
		
		this.add(new MainPanel());
		
		this.pack();
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
