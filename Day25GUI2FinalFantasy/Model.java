import java.util.ArrayList;
import java.util.List;

public class Model {
	
	public Player player1 = new Player();
	
	public Player player2 = new Player();
	
	private List<View> views = new ArrayList<View>();
	
	boolean isPlayerOnesTurn = true;
	
	Hero selectedHero = null;
	
	public Model() {
	
		player1.heroes.add(new Attacker());
		
		player1.heroes.add(new Healer());
		
		player2.heroes.add(new Balanced());
		
		player2.heroes.add(new Balanced());
		
		
		player1.startTurn();
		
		updated();
		
	}
	
	public void addView(View inView)
	{
		views.add(inView);
	}
	
	public void updated()
	{
		for(View view : views)
		{
			view.update();
		}
	}
	
	public void endTurn()
	{
		selectedHero = null;
		if(isPlayerOnesTurn)
		{
			player2.startTurn();
		}
		else
			player1.startTurn();
		
		isPlayerOnesTurn = !isPlayerOnesTurn;
		updated();
	}

	public boolean isThisHerosTurn(Hero myHero) {
		return player1.heroes.contains(myHero) == isPlayerOnesTurn;
	}
	
	public void selectHero(Hero inHero)
	{
		selectedHero = inHero;
		updated();
	}

	public Player getOtherPlayer() {
		if(isPlayerOnesTurn)
			return player2;
		else
			return player1;
	
	}
	
	public Player getThisPlayer() {
		if(!isPlayerOnesTurn)
			return player2;
		else
			return player1;
	
	}

	public void attacks(Hero thisHero) {
		thisHero.takeDamage(selectedHero.attack);
		selectedHero.canAttack = false;
		updated();
		
	}

	public void special() {
		
		
		selectedHero.specialCooldown = selectedHero.coolDownTime;
		
		selectedHero.special(getThisPlayer(), getOtherPlayer());
		
		updated();
		
	}
	
	

}
