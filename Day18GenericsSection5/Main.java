import java.util.HashMap;
import java.util.Map;


public class Main {

	public static void main(String[] args) {
		
		
		Prediction[] myPredictions= {
				new Prediction("Virginia", 1),
				new Prediction("Hawaii", 64),
				new Prediction("Oklahoma", 2),
				new Prediction("North Carolina", 3),
				new Prediction("Wisconsin", 30),
				new Prediction("UNO", 63),
				new Prediction("UCLA", 44),
				new Prediction("UConn", 15)
		};
		
		print(myPredictions);
		
		String[] partyFood = {"Brussel sprouts", "Nachos", "Popcorn", "Rib eye", "Fries", "Pizza", "Kebeb", "Tacos"};
		
		System.out.println("And here's the fodd we're going to eat (except the brussel sprouts)");
		
		print(partyFood);
		/*
		 * Generics
		 * OOP in Java lets us define a class hierarchy that is general instead of sprecific.
		 * Generics let us create data structures that are general, not specific
		 */
		
		/*
		 * Generics don't work with primitves
		 * But all is not lost!
		 * 
		 * Thanks to boxing and unboxing
		 * 
		 * int -> Integer()
		 * char -> Character()
		 * double -> Double()
		 * 
		 */
		
		//int[] highestScore = new int[]{275, 220, 80, 185};
		Integer[] highestScore = {275, 220, 80, 185};
		
		System.out.println("Here's my predictions for highest scores");
		
		print(highestScore);
		
		System.out.println("Here are my predictions for biggest upsets");
		
		/*
		 * How to use classes with generic type arguments
		 * 
		 */
		BadList<String> badList = new BadList<String>();
		badList.add("Hawaii wins");
		badList.add("North Carolina");
		badList.add("Connecticut in the final four");
		
		
		for(int i = 0; i < 3; i++)
		{
			System.out.println(
					badList.get(i).substring(0, 3)
					);
		}
		
		/*
		 * Example of a class that takes multiple type parameters
		 */
		Map<String, Integer> grades = new HashMap<String, Integer>();
		grades.put("Talol", 32);
		grades.put("Kate", 95);
		grades.put("Gage", 70);
		grades.put("Cody", 300);
		System.out.println(grades.get("Cody"));
		
		/* Example of no type parameter */
		BadList otherList = new BadList();
		otherList.add("UNO wins");
		otherList.add("North Dakota");
		otherList.add("Kentucky in the final four");
		
		
		
		
		
		
		

	}

	/**
	 * Steps to generic methods
	 * 1. Tell Java that we're going to use a generic type by putting it in <>
	 * 2. Replace types with generic type
	 * @param myPredictions
	 */
	private static <T> void print(T[] myPredictions) {
		for(int i =0; i < myPredictions.length; i++)
			System.out.println(myPredictions[i].toString());
		
	}
	
	/*
	 * Extending generics
	 */
	/**
	 * To force generics to be of a certain type, use the extends keyword.
	 * 
	 * @param myPredictions
	 */
	private static <T extends HasName> void printNames(T[] myPredictions) {
		for(int i =0; i < myPredictions.length; i++)
			System.out.println(myPredictions[i].getName());
		
	}
	
	
	

}
