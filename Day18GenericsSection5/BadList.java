/**
 * Why using Object[] is bad:
 * I can add anythying I want (wrong type)
 * I have to cast everything I pull out
 * 
 * How to make a class generic:
 * 1. Add a generic type parameter
 * -This goes after the class name
 * 2. Replace class references with the new parameter
 * 
 * You should know about generics:
 * 1. You can have as many type parameters as you want
 * <T,U,V>
 * 
 * 2. You don't have to pass in type arguments (but Java assumes you're going to use Objects).
 * (You slhould probably never do this!)
 */
public class BadList<T> {
	
	/*
	 * How to declare generic arrays
	 */
//	T[] entries = new T[10];
	T[] entries = (T[])new Object[10];
	int nextEntry = 0;
	
	public void add(T s)
	{
		entries[nextEntry] = s;
		nextEntry++;
	}
	
	public T get(int i)
	{
		return entries[i];
	}

}
